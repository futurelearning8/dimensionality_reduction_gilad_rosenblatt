import numpy as np
import matplotlib.pyplot as plt


def plot_with_two_scales(x, y1, y2, labels=None, hlines=None, vlines=None, ylimits=None):
    """
    Plot two curves y1(x) and y2(x) in the same x-axis but each having its own y-axis.

    Docs on plot w/different scales: https://matplotlib.org/stable/gallery/subplots_axes_and_figures/two_scales.html.

    :param np.ndarray x: x-axis values to plot for.
    :param np.ndarray y1: y-axis values of y1(x) to plot.
    :param np.ndarray y2: y-axis values of y2(x) to plot.
    :param dict labels: dictionary with keys "x" for x label, "y" for list with y1 and y2 labels and "title" for title.
    :param list hlines: list of 2 lists for values to plot as horizontal lines on the y1-axis and y2-axis.
    :param list vlines: list of values to plot as vertical lines on the x-axis.
    :return tuple: the two axis for y1(x) and y2(x) plots.
    """

    # Create a new figure and axis.
    fig, ax1 = plt.subplots(nrows=1, ncols=1)
    color1 = "tab:red"

    # Instantiate a second axes that shares the same x-axis.
    ax2 = ax1.twinx()
    color2 = "tab:blue"

    # Add grid lines (in the background) and set axis limits.
    for ax, ylimit in zip([ax1, ax2], ylimits):  # Zip will return empty if ylimits == None.
        ax.set_ylim(ylimit)

    # Plot horizontal reference lines if any are provided (in the background).
    if hlines:
        assert len(hlines) == 2
        for index, (ax, color) in enumerate(zip([ax1, ax2], [color1, color2])):
            for value in hlines[index]:
                ax.axhline(value, color=color, linestyle="dashed", linewidth=1)
    for value in vlines:
        ax1.axvline(value, color="C7", linestyle="dashed", linewidth=1)  # No matter if axis is 1 or 2.

    # Plot y1(x).
    ax1.plot(x, y1, color=color1, marker=".", linewidth=2, markersize=12)
    ax1.tick_params(axis='y', labelcolor=color1)

    # Plot y2(x).
    ax2.plot(x, y2, color=color2, marker=".", linewidth=2, markersize=12)
    ax2.tick_params(axis='y', labelcolor=color2)

    # Annotate axes according to input labels.
    if labels:
        for key, label in labels.items():
            if key == "x":
                ax1.set_xlabel(label)  # No matter if axis is 1 or 2.
            elif key == "y":
                assert len(label) == 2
                ax1.set_ylabel(label[0], color=color1)
                ax2.set_ylabel(label[1], color=color2)
            elif key == "title":
                ax1.set_title(label)  # No matter if axis is 1 or 2.

    # Avoid clipping and draw.
    fig.tight_layout()
    plt.draw()

    # Return axes for this plot.
    return ax1, ax2


if __name__ == "__main__":

    x = np.array([5, 7, 11, 17, 26, 44, 87])
    y1 = np.array([74.73, 87.22, 93.59, 96.51, 97.39, 97.42, 97.33])
    y2 = np.array([33.23, 40.81, 50.92, 60.74, 70.02, 80.33, 90.01])
    h_val = [96.88]

    _ = plot_with_two_scales(
        x=x,
        y1=y1,
        y2=y2,
        vlines=list(x),
        hlines=[[h_val], []],
        ylimits=[[0, 100], [0, 100]],
        labels={
            "x": "Number of principal components",
            "y": ["KNN accuracy score [%]", "PCA variance explained [%]"],
            "title": "How does PCA affects KNN accuracy on MNIST data?"
        }
    )

    plt.show()
