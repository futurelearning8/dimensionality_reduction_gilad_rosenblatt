import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from plotters import plot_with_two_scales


def load_data():
    """
    Loads the MNIST database using tensorflow.On how to load the MNIST dataset using tensorflow see:
    https://stackoverflow.com/questions/50666681/how-to-load-mnist-via-tensorflow-including-download/50669146#50669146].
    Loaded feature data has dimensions (n_images, row_pixels, col_pixels).

    :return tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]: MNIST train and test (3D) feature and label arrays.
    """
    (X_train, y_train), (X_test, y_test) = tf.keras.datasets.mnist.load_data()
    return X_train, y_train, X_test, y_test


def reshape_data(X_train, y_train, X_test, y_test):
    """
    Reshape image train and test sets to dimensions that feed into models:
    (n_images, row_pixels, col_pixels) --> (n_images, row_pixels * col_pixels)

    :param np.ndarray X_train: 3D feature array of train set.
    :param np.ndarray y_train: 1D label array of train set (not transformed, here for pipelining syntax).
    :param np.ndarray X_test: 3D feature array of test set.
    :param np.ndarray y_test: 1D label array of test set (not transformed, here for pipelining syntax).
    :return tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]: 2D train and test feature arrays and 1D labels.
    """
    return X_train.reshape(X_train.shape[0], -1), y_train, X_test.reshape(X_test.shape[0], -1), y_test


def do_pca(X_train, y_train, X_test, y_test, n_components=10):
    """
    Project train and test sets using principal component analysis for a given number of components.
    
    :param np.ndarray X_train: 2D feature array of train set.
    :param np.ndarray y_train: 1D label array of train set (not transformed, here for pipelining syntax).
    :param np.ndarray X_test: 2D feature array of test set.
    :param np.ndarray y_test: 1D label array of test set (not transformed, here for pipelining syntax).
    :param int n_components: number of principal components to use for PCA model.
    :return tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]: projected train and test sets with labels.
    """

    # Take a PCA model with n_components.
    pca = PCA(n_components=n_components)

    # Fit the model to the train set and transform both train and test sets.
    X_train_projected = pca.fit_transform(X_train)
    X_test_projected = pca.transform(X_test)

    # Return the PCA-ed datasets.
    return X_train_projected, y_train, X_test_projected, y_test


# TODO hyperparameter tune and evaluate using GridScoreCV?
def train_model(X_train, y_train, X_test, y_test):
    """
    Train and evaluate a k-nearest neighbors classifier on input data.

    :param np.ndarray X_train: 2D feature array of train set.
    :param np.ndarray y_train: 1D label array of train set.
    :param np.ndarray X_test: 2D feature array of test set.
    :param np.ndarray y_test: 1D label array of test set.
    """

    # Fit a k-nearest neighbors model to training set.
    model = KNeighborsClassifier(n_neighbors=5)
    model.fit(X_train, y_train)

    # Use the fitted model to predict labels on the test set.
    y_test_preds = model.predict(X_test)

    # Return the model accuracy score.
    return accuracy_score(y_test, y_test_preds)


def find_n_components(X, variance_objectives=np.arange(0.3, 1, 0.1)):
    """
    Find number of components a principal component decomposition needs to explain given variance levels on input data.

    :param np.array X: data used to train a principal component decomposition model
    :param np.array variance_objectives: explained variance marks required for a principal component decomposition on X.
    :return np.array: the number of components a principal component decomposition on X needs to meet each objective.
    """

    # Perform principal component decomposition with number of components being the same as original number of features.
    pca = PCA(n_components=X.shape[1])
    pca.fit(X)

    # Calculate the monotonically increasing array of variance explained by n components (n_components = index + 1).
    explained_variances = np.cumsum(pca.explained_variance_ratio_)

    # Find where variance objectives are met in sorted explained-variance array.
    indices = np.searchsorted(explained_variances, variance_objectives)

    # Return the minimal number of components a PCA needs to meet each variance objective and its explained variance.
    return indices + 1, explained_variances[indices]


def main():
    # Load MNIST data in trainable form (2D feature arrays).
    X_train, y_train, X_test, y_test = reshape_data(*load_data())

    # Calculate the number of components a PCA model will need to meet each explained variance objective.
    num_components, explained_variances = find_n_components(X=X_train, variance_objectives=np.arange(0.3, 1, 0.1))

    # Train and evaluate a k-nearest neighbors model on the MNIST dataset.
    original_score = train_model(X_train, y_train, X_test, y_test)

    # Train and evaluate a k-nearest neighbors model on the PCA-ed dataset for each variance objective.
    scores = []
    for this_n_components in num_components:
        scores.append(train_model(*do_pca(X_train, y_train, X_test, y_test, n_components=this_n_components)))

    # Report the results to console.
    print("--------------------------------------- KNN -----------------------------------------------")
    print(f"A KNN model on MNIST dataset scored {100 * original_score:.2f}% accurate.")
    print("--------------------------------------- KNN + PCA -----------------------------------------")
    for n_components, explained_variance, score in zip(list(num_components), list(explained_variances), scores):
        print(
            f"Using {n_components} components "
            f"that explain {100 * explained_variance:.2f}% of the variance "
            f"KNN scored {100 * score:.2f}% accurate."
        )
    print("-------------------------------------------------------------------------------------------")

    # Generate a summary image.
    _ = plot_with_two_scales(
        x=num_components,
        y1=np.array(scores) * 100,
        y2=explained_variances * 100,
        vlines=list(num_components),
        hlines=[[original_score * 100], []],
        ylimits=[[0, 100], [0, 100]],
        labels={
            "x": "Number of principal components",
            "y": ["KNN accuracy score [%]", "PCA variance explained [%]"],
            "title": "How does PCA affects KNN accuracy on MNIST data?"
        }
    )
    plt.savefig("../images/knn_pca_scores.png")


if __name__ == "__main__":
    main()
